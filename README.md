## Requirements
- Python 2.7

## Setup

```
pip install -r requirements
```

## Running
```
python manage.py runserver
python manage.py tests
```
