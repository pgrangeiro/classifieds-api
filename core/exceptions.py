class InvalidCategoryObject(Exception):
    '''
        Exception to be raised if an object is not an category instance.
    '''
