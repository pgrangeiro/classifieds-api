# coding: utf-8
from core.presenters import NullablePresenter
from core.repositories import CategoryRepository


class ListCategoryUseCase(object):

    def __init__(self, dao, presenter=None):
        self.repository = CategoryRepository(dao)
        self.presenter = presenter or NullablePresenter

    def execute(self, category_ids=None, label=None, slug=None, parent_ids=None):
        category_ids = category_ids or []
        parent_ids = parent_ids or []

        data = self.repository.list(
            category_ids=category_ids,
            label=label,
            slug=slug,
            parent_ids=parent_ids,
        )
        for item in data:
            yield self.presenter.format(item)
