# coding: utf-8


class DeleteCategoryUseCase(object):

    def __init__(self, dao):
        self.dao = dao

    def execute(self, category_id):
        self.dao.delete(category_id=category_id)
