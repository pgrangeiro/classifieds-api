# coding: utf-8


class CreateCategoryUseCase(object):

    def __init__(self, dao):
        self.dao = dao

    def execute(self, label, slug, parent_id=None):
        self.dao.create(
            label=label,
            slug=slug,
            parent_id=parent_id,
        )
