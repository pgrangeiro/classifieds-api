# coding: utf-8


class UpdateCategoryUseCase(object):

    def __init__(self, dao):
        self.dao = dao

    def execute(self, category_id, label, slug, parent_id):
        self.dao.update(
            category_id=category_id,
            label=label,
            slug=slug,
            parent_id=parent_id,
        )
