# coding: utf-8
from core.domain import Category


class CategoryFactory(object):

    @classmethod
    def create(cls, id, label, slug, items=None, parent_slug=None):
        items = items or []

        obj = Category(
            id=id,
            label=label,
            slug=slug,
            parent_slug=parent_slug,
        )
        for item in items:
            obj.add_item(cls.create(**item))
        return obj
