# coding: utf-8
from core.exceptions import InvalidCategoryObject


class Category(object):

    def __init__(self, id, label, slug, parent_slug=None):
        self.id = id
        self.label = label
        self.slug = slug
        self.url = self.__make_url(parent_slug)
        self.items = []

    def add_item(self, category):
        if not isinstance(category, self.__class__):
            raise InvalidCategoryObject
        self.items.append(category)

    def __make_url(self, parent_slug=None):
        if parent_slug:
            return '#/%s/%s' % (parent_slug, self.slug)
        return '#/%s' % self.slug

