# coding: utf-8
from unittest import TestCase

from core.domain import Category


class CategoryTestCase(TestCase):

    def setUp(self):
        self.data = {
            'id': 1,
            'label': 'Category',
            'slug': 'slug',
        }

    def test_instantiate_obj_correctly(self):
        obj = Category(**self.data)
        self.assertEqual(obj.id, self.data['id'])
        self.assertEqual(obj.label, self.data['label'])
        self.assertEqual(obj.slug, self.data['slug'])
        self.assertEqual(obj.url, '#/slug')

    def test_instantiate_obj_with_parent_slug_correctly(self):
        self.data['parent_slug'] = 'parent_slug'

        obj = Category(**self.data)
        self.assertEqual(obj.id, self.data['id'])
        self.assertEqual(obj.label, self.data['label'])
        self.assertEqual(obj.slug, self.data['slug'])
        self.assertEqual(obj.url, '#/parent_slug/slug')
