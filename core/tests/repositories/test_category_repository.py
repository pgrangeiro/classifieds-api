# coding: utf-8
from mock import call, patch, Mock
from unittest import TestCase

from core.repositories import CategoryRepository


class CategoryRepositoryTestCase(TestCase):

    def setUp(self):
        self.dao = Mock()
        self.dao.filter.return_value = [{'key': 1}, {'key': 2}]
        self.dao.all.return_value = [{'key': 1}, {'key': 2}, {'key': 3}, {'key': 4}]
        self.dao.get.return_value = {'param': 1}

        self.mocked_factory = patch('core.repositories.category_repository.CategoryFactory').start()

        self.repository = CategoryRepository(self.dao)

    def test_get_calls_dao_correctly(self):
        self.repository.get(1)
        self.dao.get.assert_called_once_with(1)

    def test_get_calls_factory_correctly(self):
        self.mocked_factory.create.return_value = 1

        data = self.repository.get(1)
        self.mocked_factory.create.assert_called_once_with(param=1)
        self.assertEqual(1, data)

    def test_list_calls_dao_correctly(self):
        self.mocked_factory.create.side_effect = [1, 2]

        values = list(self.repository.list(category_ids=[1, 2]))

        self.dao.filter.assert_called_once_with(category_ids=[1, 2])
        self.assertEqual(2, self.mocked_factory.create.call_count)
        self.mocked_factory.create.assert_has_calls([
            call(key=1),
            call(key=2),
        ])
        self.assertEqual([1, 2], values)

    def test_list_without_kwargs_calls_dao_correctly(self):
        expected = [1, 2, 3, 4]
        self.mocked_factory.create.side_effect = expected

        values = list(self.repository.list())

        self.assertTrue(self.dao.all.called)
        self.assertEqual(4, self.mocked_factory.create.call_count)
        self.mocked_factory.create.assert_has_calls([
            call(key=1),
            call(key=2),
            call(key=3),
            call(key=4),
        ])
        self.assertEqual(expected, values)
