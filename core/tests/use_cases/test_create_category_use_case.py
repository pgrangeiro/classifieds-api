# coding: utf-8
from mock import Mock
from unittest import TestCase

from core.use_cases import CreateCategoryUseCase


class CreateCategoryUseCaseTestCase(TestCase):

    def setUp(self):
        self.dao = Mock()
        self.use_case = CreateCategoryUseCase(self.dao)

    def test_initializes_use_case_correctly(self):
        self.assertEqual(self.dao, self.use_case.dao)

    def test_execute_calls_dao_correctly(self):
        self.use_case.execute('label', 'label-slug', 2)
        self.dao.create.assert_called_once_with(
            label='label',
            slug='label-slug',
            parent_id=2,
        )

    def test_execute_calls_dao_without_parent_correctly(self):
        self.use_case.execute('label', 'label-slug')
        self.dao.create.assert_called_once_with(
            label='label',
            slug='label-slug',
            parent_id=None,
        )
