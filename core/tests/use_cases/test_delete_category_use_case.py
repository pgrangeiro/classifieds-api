# coding: utf-8
from mock import Mock
from unittest import TestCase

from core.use_cases import DeleteCategoryUseCase


class DeleteCategoryUseCaseTestCase(TestCase):

    def setUp(self):
        self.dao = Mock()
        self.use_case = DeleteCategoryUseCase(self.dao)

    def test_initializes_use_case_correctly(self):
        self.assertEqual(self.dao, self.use_case.dao)

    def test_execute_calls_dao_correctly(self):
        self.use_case.execute(1)
        self.dao.delete.assert_called_once_with(
            category_id=1,
        )
