# coding: utf-8
from mock import call, patch, Mock
from unittest import TestCase

from core.presenters import NullablePresenter
from core.use_cases import ListCategoryUseCase


class ListCategoryUseCaseTestCase(TestCase):

    def setUp(self):
        patcher = patch('core.use_cases.list_category_use_case.CategoryRepository')
        self.repository = patcher.start()
        self.addCleanup(patcher.stop)

        self.dao = Mock()
        self.use_case = ListCategoryUseCase(self.dao)

    def test_initializes_use_case_correctly(self):
        self.repository.reset_mock()
        use_case = ListCategoryUseCase(self.dao)

        self.repository.assert_called_once_with(self.dao)
        self.assertEqual(self.repository(), use_case.repository)
        self.assertEqual(NullablePresenter, use_case.presenter)

    def test_initializes_use_case_with_presenter_correctly(self):
        self.repository.reset_mock()
        presenter = Mock()
        use_case = ListCategoryUseCase(self.dao, presenter)

        self.repository.assert_called_once_with(self.dao)
        self.assertEqual(self.repository(), use_case.repository)
        self.assertEqual(presenter, use_case.presenter)

    def test_execute_calls_repository_correctly(self):
        list(self.use_case.execute([1, 2], 'label', 'slug', [3]))
        self.repository().list.assert_called_once_with(
            category_ids=[1, 2],
            label='label',
            slug='slug',
            parent_ids=[3],
        )

    def test_execute_calls_repository_correctly_without_category_ids_param(self):
        list(self.use_case.execute([]))
        self.repository().list.assert_called_once_with(
            category_ids=[],
            label=None,
            slug=None,
            parent_ids=[],
        )

    def test_execute_calls_presenter_correctly(self):
        presenter = Mock()
        presenter.format.side_effect = ['1', '2']
        self.use_case.presenter = presenter
        self.repository().list.return_value = [1, 2]

        data = list(self.use_case.execute([]))

        self.assertEqual(2, presenter.format.call_count)
        presenter.format.assert_has_calls([call(1), call(2)])
        self.assertEqual(['1', '2'], data)


