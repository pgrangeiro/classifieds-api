# coding: utf-8
from unittest import TestCase

from core.presenters import NullablePresenter


class NullablePresenterTestCase(TestCase):

    def test_formats_returns_none(self):
        self.assertIsNone(NullablePresenter.format([]))
