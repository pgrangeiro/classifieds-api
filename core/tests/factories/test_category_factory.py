# coding: utf-8
from unittest import TestCase

from core.domain import Category
from core.factories import CategoryFactory


class CategoryFactoryTestCase(TestCase):

    def setUp(self):
        self.data = {
            'id': 1,
            'label': 'Category',
            'slug': 'slug',
        }

    def test_create_object_correctly(self):
        obj = CategoryFactory.create(**self.data)

        self.assertIsInstance(obj, Category)
        self.assertEqual(obj.id, self.data['id'])
        self.assertEqual(obj.label, self.data['label'])
        self.assertEqual(obj.slug, self.data['slug'])
        self.assertEqual(0, len(obj.items))

    def test_create_object_with_parent_slug_correctly(self):
        self.data['parent_slug'] = 'XPTO'
        obj = CategoryFactory.create(**self.data)

        self.assertIsInstance(obj, Category)
        self.assertEqual(obj.id, self.data['id'])
        self.assertEqual(obj.label, self.data['label'])
        self.assertEqual(obj.slug, self.data['slug'])
        self.assertEqual(0, len(obj.items))

    def test_create_object_with_subcategories_correctly(self):
        items = [{
            'id': 2,
            'label': 'Category2',
            'slug': 'slug2',
        }, {
            'id': 3,
            'label': 'Category3',
            'slug': 'slug3',
        }]
        self.data['items'] = items

        obj = CategoryFactory.create(**self.data)

        self.assertEqual(2, len(obj.items))
        for index, item in enumerate(obj.items):
            self.assertIsInstance(item, Category)
            self.assertEqual(item.id, self.data['items'][index]['id'])
            self.assertEqual(item.label, self.data['items'][index]['label'])
            self.assertEqual(item.slug, self.data['items'][index]['slug'])
        self.assertEqual(0, len(item.items))
