# coding:utf-8
from core.factories import CategoryFactory


class CategoryRepository(object):

    def __init__(self, dao):
        self.dao = dao

    def list(self, **kwargs):
        if any(kwargs.values()):
            data = self.dao.filter(**kwargs)
        else:
            data = self.dao.all()
        for item in data:
            yield CategoryFactory.create(**item)

    def get(self, category_id):
        data = self.dao.get(category_id)
        return CategoryFactory.create(**data)
