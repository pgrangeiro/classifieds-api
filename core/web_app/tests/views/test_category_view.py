# coding: utf-8
import json

from copy import deepcopy
from model_mommy import mommy

from core.web_app.models import Category
from django.core.urlresolvers import reverse
from django.test import TestCase


class CategoryListView(TestCase):

    def setUp(self):
        self.url = reverse('core:category_list')

        self.categories = [{
            'id': 1,
            'label': u'XPTO',
            'slug': u'xpto',
        }, {
            'id': 2,
            'label': u'XPTO2',
            'slug': u'xpto2',
        }]
        for item in self.categories:
            mommy.make(Category, **item)

    def test_get_calls_use_case_correctly(self):
        expected = [{
            'id': 1,
            'label': u'XPTO',
            'url': u'#/xpto',
            'items': [],
        }, {
            'id': 2,
            'label': u'XPTO2',
            'url': u'#/xpto2',
            'items': [],
        }]

        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        data = json.loads(response.content)
        self.assertEqual(2, len(data))
        for item in data:
            self.assertIn(item, expected)
