# coding: utf-8
from model_mommy import mommy

from core.web_app.models import Category
from django.test import TestCase


class CategoryTestCase(TestCase):

    def test_parents_filters_only_parent_categories(self):
        expected = [
            mommy.make(Category),
            mommy.make(Category),
        ]
        not_expected = mommy.make(Category, parent=expected[0])

        qs = Category.objects.parents()
        self.assertEqual(2, qs.count())
        self.assertIn(expected[0], qs)
        self.assertIn(expected[1], qs)
