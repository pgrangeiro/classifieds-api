# coding: utf-8
from mock import call, patch, Mock
from unittest import TestCase

from core.web_app.daos import CategoryDao
from core.web_app.exceptions import ParentCategoryDoesNotExist
from core.web_app.models import Category


class CategoryDaoTestCase(TestCase):

    def setUp(self):
        self.mocked_class = patch('core.web_app.daos.category_dao.Category', Mock(Category)).start()

        self.dao = CategoryDao()
        self.mocked_mapper = Mock(self.dao.mapper)
        self.dao.mapper = self.mocked_mapper

    def test_create_calls_orm_correctly(self):
        self.dao.create(
            label='Label',
            slug='category-slug',
        )
        self.mocked_class.objects.create.assert_called_once_with(
            label='Label',
            slug='category-slug',
        )

    def test_create_calls_orm_correctly_with_parent_category(self):
        expected = Category(id=2)
        self.mocked_class.objects.get.return_value = expected

        self.dao.create(
            label='Label',
            slug='category-slug',
            parent_id=2,
        )

        self.mocked_class.objects.create.assert_called_once_with(
            label='Label',
            slug='category-slug',
            parent=expected,
        )

    def test_create_raises_exception_when_parent_category_does_not_exist(self):
        self.mocked_class.DoesNotExist = Category.DoesNotExist
        self.mocked_class.objects.get.side_effect = Category.DoesNotExist

        self.assertRaises(ParentCategoryDoesNotExist, self.dao.create, label='label', slug='slug', parent_id=99)

    def test_update_calls_orm_correctly(self):
        self.dao.update(
            category_id=1,
            label='Label',
            slug='category-slug',
        )
        self.mocked_class.objects.filter.assert_called_once_with(id=1)
        self.mocked_class.objects.filter().update.assert_called_once_with(
            label='Label',
            slug='category-slug',
        )

    def test_update_calls_orm_correctly_with_parent_id_param(self):
        expected = Category(id=2)
        self.mocked_class.objects.get.return_value = expected

        self.dao.update(
            category_id=1,
            label='Label',
            slug='category-slug',
            parent_id=2,
        )
        self.mocked_class.objects.filter.assert_called_once_with(id=1)
        self.mocked_class.objects.filter().update.assert_called_once_with(
            label='Label',
            slug='category-slug',
            parent=expected,
        )

    def test_update_raises_exception_when_parent_category_does_not_exist(self):
        self.mocked_class.DoesNotExist = Category.DoesNotExist
        self.mocked_class.objects.get.side_effect = Category.DoesNotExist

        self.assertRaises(ParentCategoryDoesNotExist, self.dao.update, category_id=1, label='label', slug='slug', parent_id=99)

    def test_delete_calls_orm_correctly(self):
        self.dao.delete(1)
        self.mocked_class.objects.filter.assert_called_once_with(id=1)
        self.assertTrue(self.mocked_class.objects.filter().delete.called)

    def test_all_calls_orm_correctly(self):
        expected = [
            Category(id=1, label='XPTO', slug='xpto'),
            Category(id=2, label='XPTO2', slug='xpto2'),
        ]
        self.mocked_class.objects.parents().all.return_value = expected

        list(self.dao.all())
        self.assertTrue(self.mocked_class.objects.parents().all.called)

    def test_all_calls_mapper_correctly(self):
        expected = [
            Category(id=1, label='XPTO', slug='xpto'),
            Category(id=2, label='XPTO2', slug='xpto2'),
        ]
        self.mocked_class.objects.parents().all.return_value = expected
        self.mocked_mapper.format.side_effect = [1, 2]

        data = list(self.dao.all())

        calls = [call(expected[0]), call(expected[1])]
        self.mocked_mapper.format.assert_has_calls(calls)
        self.assertEqual([1, 2], data)

    def test_filter_calls_orm_correctly(self):
        expected = [
            Category(id=1, label='XPTO', slug='xpto'),
            Category(id=2, label='XPTO2', slug='xpto2'),
        ]
        self.mocked_class.objects.parents().filter.return_value = expected

        list(self.dao.filter(category_ids=[1], label='XPTO', slug='xpto', parent_ids=[1]))
        self.mocked_class.objects.parents().filter.assert_called_once_with(
            id__in=[1],
            label__icontains='XPTO',
            slug__icontains='xpto',
            parent_id__in=[1],
        )

    def test_filter_calls_mapper_correctly(self):
        expected = [
            Category(id=1, label='XPTO', slug='xpto'),
            Category(id=2, label='XPTO2', slug='xpto2'),
        ]
        self.mocked_class.objects.parents().filter.return_value = expected
        self.mocked_mapper.format.side_effect = [1, 2]

        data = list(self.dao.filter())

        calls = [call(expected[0]), call(expected[1])]
        self.mocked_mapper.format.assert_has_calls(calls)
        self.assertEqual([1, 2], data)

    def test_get_calls_orm_correctly(self):
        expected = Category(id=1, label='XPTO', slug='xpto')
        self.mocked_class.objects.get.return_value = expected

        self.dao.get(category_id=1)
        self.mocked_class.objects.get.assert_called_once_with(id=1)

    def test_get_calls_mapper_correctly(self):
        expected = Category(id=1, label='XPTO', slug='xpto')
        self.mocked_class.objects.get.return_value = expected
        self.mocked_mapper.format.return_value = 1

        data = self.dao.get(category_id=1)
        self.mocked_mapper.format.assert_called_once_with(expected)
        self.assertEqual(1, data)


class CategoryMapperTestCase(TestCase):

    def setUp(self):
        self.dao = CategoryDao()

    def test_mapper_format_instance_correctly(self):
        instance = Category(id=1, label='XPTO', slug='xpto')

        data = self.dao.mapper.format(instance)

        self.assertEqual(data['id'], instance.id)
        self.assertEqual(data['label'], instance.label)
        self.assertEqual(data['slug'], instance.slug)
        self.assertEqual(data['items'], [])
        self.assertEqual(data['parent_slug'], '')

    def test_mapper_format_instance_with_items_correctly(self):
        expected = [{
            'id': 2,
            'label': 'XPTO2',
            'slug': 'slug2',
            'parent_slug': 'xpto',
        }, {
            'id': 3,
            'label': 'XPTO3',
            'slug': 'slug3',
            'parent_slug': 'xpto',
        }]

        parent = Category(id=1, label='XPTO', slug='xpto')
        for item in expected:
            parent.category_set.add(Category(
                item['id'],
                item['label'],
                item['slug'],
            ), bulk=False)

        data = self.dao.mapper.format(parent)

        self.assertEqual(2, len(data['items']))
        for index, item in enumerate(data['items']):
            self.assertEqual(expected[index]['id'], item['id'])
            self.assertEqual(expected[index]['label'], item['label'])
            self.assertEqual(expected[index]['slug'], item['slug'])
