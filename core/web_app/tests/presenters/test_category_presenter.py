# coding: utf-8
from unittest import TestCase

from core.domain import Category
from core.web_app.presenters import CategoryPresenter


class CategoryPresenterTestCase(TestCase):

    def setUp(self):
        self.data = {
            'id': 1,
            'label': 'Category',
            'slug': 'slug',
        }

    def test_formats_obj_correctly(self):
        obj = Category(**self.data)
        formatted = CategoryPresenter.format(obj)

        self.assertEqual(formatted['id'], obj.id)
        self.assertEqual(formatted['label'], obj.label)
        self.assertEqual(formatted['url'], obj.url)
        self.assertEqual(formatted['items'], [])

    def test_formats_obj_with_items_correctly(self):
        item_data = [{
            'id': 1,
            'label': 'label',
            'slug': 'slug',
            'parent_slug': 'slug',
        }, {
            'id': 2,
            'label': 'label2',
            'slug': 'slug2',
            'parent_slug': 'slug',
        }]
        obj = Category(**self.data)
        for item in item_data:
            obj.add_item(Category(**item))

        formatted = CategoryPresenter.format(obj)

        self.assertEqual(2, len(formatted['items']))
        for index, item in enumerate(formatted['items']):
            self.assertEqual(obj.items[index].id, item['id'])
            self.assertEqual(obj.items[index].label, item['label'])
            self.assertEqual(obj.items[index].url, item['url'])


