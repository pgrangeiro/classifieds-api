# coding: utf-8
from django.test import TestCase
from django.utils.translation import ugettext as _

from core.web_app.forms import CategoryForm


class CategoryFormTestCase(TestCase):

    def setUp(self):
        self.data = {
            'label': 'XPTO',
            'slug': 'xpto',
        }

    def test_required_fields(self):
        form = CategoryForm({})

        self.assertFalse(form.is_valid())
        self.assertEqual(_(u'This field is required.'), form.errors['label'][0])
        self.assertEqual(_(u'This field is required.'), form.errors['slug'][0])

    def test_form_data_is_valid(self):
        form = CategoryForm(self.data)
        self.assertTrue(form.is_valid())

    def test_form_data_with_not_required_fields_is_valid(self):
        self.data['parent_id'] = 1
        self.data['category_id'] = 2

        form = CategoryForm(self.data)
        self.assertTrue(form.is_valid())
