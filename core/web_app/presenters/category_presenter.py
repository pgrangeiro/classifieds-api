# coding: utf-8
from copy import deepcopy


class CategoryPresenter(object):

    @classmethod
    def format(self, obj):
        data = deepcopy(obj.__dict__)

        items = []
        for item in data['items']:
            items.append(item.__dict__)
        data.pop('slug')
        data['items'] = items

        return data
