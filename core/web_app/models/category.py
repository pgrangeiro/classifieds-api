# coding:utf-8
from django.db import models


class CategoryManager(models.Manager):

    def parents(self, **kwargs):
        return self.filter(parent__isnull=True, **kwargs)


class Category(models.Model):

    objects = CategoryManager()

    label = models.CharField(max_length=100)
    slug = models.CharField(max_length=125)
    parent = models.ForeignKey('self', null=True)

    class Meta:
        app_label = 'core'
