class ParentCategoryDoesNotExist(Exception):
    '''
        Exception to be raised when parent category does not exist
    '''
