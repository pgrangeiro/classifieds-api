# coding: utf-8
from braces.views import JSONResponseMixin

from core.use_cases import ListCategoryUseCase
from core.web_app.daos import CategoryDao
from core.web_app.presenters import CategoryPresenter
from django.views.generic import View, ListView


class CategoryListView(JSONResponseMixin, ListView):

    def get(self, request, *args, **kwargs):
        use_case = ListCategoryUseCase(CategoryDao(), CategoryPresenter)
        data = list(use_case.execute())
        return super(CategoryListView, self).render_json_response(data)


class CategoryView(JSONResponseMixin, View):

    def post(self):
        pass

    def put(self):
        pass

    def get(self):
        return super(CategoryView, self).render_json_object_response([])

    def delete(self):
        pass
