from django.conf.urls import url

from core.web_app.views import CategoryListView, CategoryView

urlpatterns = [
    url(r'category/$', CategoryListView.as_view(), name='category_list'),
    url(r'category/?P(category_id)\+d/$', CategoryView.as_view(), name='category_manage'),
]
