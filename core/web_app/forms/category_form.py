# coding: utf-8
from django import forms


class CategoryForm(forms.Form):

    category_id = forms.IntegerField(required=False)
    label = forms.CharField(max_length=100)
    slug = forms.CharField(max_length=125)
    parent_id = forms.IntegerField(required=False)
