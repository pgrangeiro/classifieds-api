# coding: utf-8
from core.web_app.exceptions import ParentCategoryDoesNotExist
from core.web_app.models import Category


class CategoryMapper(object):

    @classmethod
    def format(cls, obj):
        items = []
        for item in obj.category_set.all():
            items.append(cls.format(item))

        parent_slug = ''
        if obj.parent:
            parent_slug = obj.parent.slug

        return {
            'id': obj.id,
            'label': obj.label,
            'slug': obj.slug,
            'items': items,
            'parent_slug': parent_slug,
        }


class CategoryDao(object):

    mapper = CategoryMapper

    def create(self, **kwargs):
        parent_id = kwargs.pop('parent_id', None)
        if parent_id:
            try:
                kwargs['parent'] = Category.objects.get(id=parent_id)
            except Category.DoesNotExist:
                raise ParentCategoryDoesNotExist
        Category.objects.create(**kwargs)

    def filter(self, **kwargs):
        if kwargs.get('category_ids', None):
            kwargs['id__in'] = kwargs.pop('category_ids')
        if kwargs.get('label', None):
            kwargs['label__icontains'] = kwargs.pop('label')
        if kwargs.get('slug', None):
            kwargs['slug__icontains'] = kwargs.pop('slug')
        if kwargs.get('parent_ids', None):
            kwargs['parent_id__in'] = kwargs.pop('parent_ids')

        qs = Category.objects.parents().filter(**kwargs)
        for instance in qs:
            yield self.mapper.format(instance)

    def all(self):
        qs = Category.objects.parents().all()
        for instance in qs:
            yield self.mapper.format(instance)

    def get(self, category_id):
        instance = Category.objects.get(id=category_id)
        return self.mapper.format(instance)

    def update(self, category_id, **kwargs):
        parent_id = kwargs.pop('parent_id', None)
        if parent_id:
            try:
                kwargs['parent'] = Category.objects.get(id=parent_id)
            except Category.DoesNotExist:
                raise ParentCategoryDoesNotExist
        Category.objects.filter(id=category_id).update(**kwargs)

    def delete(self, category_id):
        Category.objects.filter(id=category_id).delete()
